package br.jus.stf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exemplo12FatoresApplication {

	public static void main(String[] args) {
		SpringApplication.run(Exemplo12FatoresApplication.class, args);
	}
	
}

