package br.jus.stf.servidor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ServidorRestService {

	@Value("${servicos.servidores.endereco}")
	private String enderecoDoServidor;

	
	public Servidor recuperaPelaMatricula(Long matricula) {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForEntity(url("/api/servidor/{matricula}"), Servidor.class, matricula)
				.getBody();
	}


	private String url(String uri) {
		return enderecoDoServidor+uri;
	}
}
