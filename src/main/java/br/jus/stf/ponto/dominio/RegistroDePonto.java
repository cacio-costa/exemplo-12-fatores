package br.jus.stf.ponto.dominio;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "registro_de_ponto", schema = "ponto")
public class RegistroDePonto {

	@Id
	private Long id;
	private LocalDateTime registro;
	
	@Column(name = "matricula_servidor")
	private Long matriculaDoServidor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getRegistro() {
		return registro;
	}

	public void setRegistro(LocalDateTime registro) {
		this.registro = registro;
	}

	public Long getMatriculaDoServidor() {
		return matriculaDoServidor;
	}

	public void setMatriculaDoServidor(Long matriculaDoServidor) {
		this.matriculaDoServidor = matriculaDoServidor;
	}
	
}
