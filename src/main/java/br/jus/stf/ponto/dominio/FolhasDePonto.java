package br.jus.stf.ponto.dominio;

import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.jus.stf.servidor.Servidor;
import br.jus.stf.servidor.ServidorRestService;

@Component
public class FolhasDePonto {

	private EntityManager em;
	private ServidorRestService servicoServidores;
	
	@Autowired
	public FolhasDePonto(EntityManager em, ServidorRestService servicoServidores) {
		this.em = em;
		this.servicoServidores = servicoServidores;
	}
	
	
	public Optional<FolhaDePonto> folhaDoServidor(Long matricula, YearMonth periodo) {
		String jpql = "select rp from RegistroDePonto rp "
					+ " where rp.matriculaDoServidor = :matriculaDoServidor "
					+ "   and year(rp.registro) = :ano "
					+ "   and month(rp.registro) = :mes "
					+ " order by rp.registro";
		
		List<RegistroDePonto> registros = em.createQuery(jpql, RegistroDePonto.class)
			.setParameter("matriculaDoServidor", matricula)
			.setParameter("ano", periodo.getYear())
			.setParameter("mes", periodo.getMonthValue())
			.getResultList();
		
		if (registros.isEmpty()) {
			return Optional.empty();
		}
		
		Servidor servidor = servicoServidores.recuperaPelaMatricula(matricula);
		return Optional.of(new FolhaDePonto(periodo, servidor, registros));
	}
	
}
