package br.jus.stf.ponto.dominio;

import java.time.YearMonth;
import java.util.List;

import br.jus.stf.servidor.Servidor;


public class FolhaDePonto {
	
	private YearMonth anoMes;
	private Servidor servidor;
	private List<RegistroDePonto> registros;
	
	public FolhaDePonto(YearMonth anoMes, Servidor servidor, List<RegistroDePonto> registros) {
		this.anoMes = anoMes;
		this.servidor = servidor;
		this.registros = registros;
	}

	
	public YearMonth getAnoMes() {
		return anoMes;
	}
	
	public Servidor getServidor() {
		return servidor;
	}
	
	public List<RegistroDePonto> getRegistros() {
		return registros;
	}
	
}