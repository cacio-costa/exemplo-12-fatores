package br.jus.stf.ponto.controller;

import java.time.YearMonth;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import br.jus.stf.ponto.dominio.FolhaDePonto;
import br.jus.stf.ponto.dominio.FolhasDePonto;

@RestController
public class FolhaController {

	private FolhasDePonto folhas;

	@Autowired
	public FolhaController(FolhasDePonto folhas) {
		this.folhas = folhas;
	}
	
	
	@GetMapping("/api/folha/{mes}-{ano}/{matricula}")
	public ResponseEntity<FolhaDePonto> listaFolhas(@PathVariable Integer mes, @PathVariable Integer ano, @PathVariable Long matricula) {
		Optional<FolhaDePonto> folha = folhas.folhaDoServidor(matricula, YearMonth.of(ano, mes));
		return ResponseEntity.of(folha);
	}
	
}
