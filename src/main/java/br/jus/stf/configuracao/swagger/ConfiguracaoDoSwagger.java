package br.jus.stf.configuracao.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class ConfiguracaoDoSwagger {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.select()
					.apis(RequestHandlerSelectors.basePackage("br.jus.stf"))
					.paths(PathSelectors.ant("/api/**"))
				.build();
	}

	private ApiInfo apiInfo() {
		Contact contato = new Contact("Cácio", null, "cacio.silva@stf.jus.br");
		
		return new ApiInfoBuilder()
				.version("1")
				.contact(contato)
				.title("Documentação do serviço de Ponto Eletrônico")
				.description("Detalha a as operações disponíveis, e como interagir com o serviço de ponto eletrônico")
				.build();
	}
	
}
