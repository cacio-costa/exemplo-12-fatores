package br.jus.stf.configuracao.wiremock;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.common.SingleRootFileSource;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;

@Configuration
@Profile("wiremock")
public class ConfiguracaoWireMock {

	@Bean
	public WireMockServer configuraWireMock() {
		WireMockServer wireMock = new WireMockServer(config());
		wireMock.start();
		
		return wireMock;
	}

	private WireMockConfiguration config() {
		return options()
			.port(8090)
			.fileSource(new SingleRootFileSource("src/main/resources"));
	}
	
}
