insert into ponto.registro_de_ponto (id, registro, matricula_servidor) values (1, to_date('2018-01-01 12:00:30', 'yyyy-MM-dd hh24:mi:ss'), 123);
insert into ponto.registro_de_ponto (id, registro, matricula_servidor) values (2, to_date('2018-01-01 19:15:43', 'yyyy-MM-dd hh24:mi:ss'), 123);
insert into ponto.registro_de_ponto (id, registro, matricula_servidor) values (3, to_date('2018-01-02 11:58:23', 'yyyy-MM-dd hh24:mi:ss'), 123);
insert into ponto.registro_de_ponto (id, registro, matricula_servidor) values (4, to_date('2018-01-02 21:30:43', 'yyyy-MM-dd hh24:mi:ss'), 123);
insert into ponto.registro_de_ponto (id, registro, matricula_servidor) values (5, to_date('2018-01-03 13:00:30', 'yyyy-MM-dd hh24:mi:ss'), 123);
insert into ponto.registro_de_ponto (id, registro, matricula_servidor) values (6, to_date('2018-01-04 20:15:43', 'yyyy-MM-dd hh24:mi:ss'), 123);
insert into ponto.registro_de_ponto (id, registro, matricula_servidor) values (7, to_date('2018-02-01 11:58:23', 'yyyy-MM-dd hh24:mi:ss'), 123);
insert into ponto.registro_de_ponto (id, registro, matricula_servidor) values (8, to_date('2018-02-01 21:30:43', 'yyyy-MM-dd hh24:mi:ss'), 123);

insert into ponto.registro_de_ponto (id, registro, matricula_servidor) values (9, to_date('2018-01-01 07:00:00', 'yyyy-MM-dd hh24:mi:ss'), 456);
insert into ponto.registro_de_ponto (id, registro, matricula_servidor) values (10, to_date('2018-01-01 14:00:00', 'yyyy-MM-dd hh24:mi:ss'), 456);
commit;