create user ponto identified by 123;
grant unlimited tablespace to ponto;

ALTER USER "SYSTEM" DEFAULT ROLE "AQ_ADMINISTRATOR_ROLE", "DBA";