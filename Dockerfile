FROM registry.stf.jus.br/centos-java-oracle:7.6.11
LABEL author="Plataforma Corporativa <G-PLTEC@stf.jus.br>"

ENV LANG=pt_BR.iso-8859-1 \
    LANGUAGE=pt_BR.iso-8859-1 \
    LC_CTYPE=pt_BR.iso-8859-1 \
    LC_ALL=pt_BR.iso-8859-1 \
    TZ=America/Sao_Paulo \
    CATALINA_HOME=/usr/apache-tomcat-6.0.53 \
    CATALINA_OPTS="-Dcom.sun.management.jmxremote \
    	-Dcom.sun.management.jmxremote.port=9010 \
    	-Dcom.sun.management.jmxremote.local.only=false \ 
    	-Dcom.sun.management.jmxremote.authenticate=false \
		-Dcom.sun.management.jmxremote.ssl=false \
		-Djava.rmi.server.hostname=127.0.0.1"

	

#RUN yum update -y; \
RUN cd /usr; \
    curl -Lkf https://registry.stf.jus.br/recursos/sistema_operacional/java/cacerts/cacerts -o $JAVA_HOME/jre/lib/security/cacerts; \
    curl -Lkf https://registry.stf.jus.br/recursos; \
    curl -LOkf https://archive.apache.org/dist/tomcat/tomcat-6/v6.0.53/bin/apache-tomcat-6.0.53.zip; \
    unzip apache-tomcat-6.0.53.zip
    
RUN rm -f apache-tomcat-6.0.53.zip; \
    chmod -R 501 /usr/apache-tomcat-6.0.53

EXPOSE 8080 9010

CMD [ "/usr/apache-tomcat-6.0.53/bin/catalina.sh", "run" ]
